#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


#pragma pack(push ,1)

#define BUFFLEN 1024
#define MAX_USER_INP 1020
#define ENDING ".bmp"


//BMP file structure
typedef struct
{
    uint16_t signature;           //2 bytes: 'BM', Magic identifier: 0x4D42
    uint32_t fileSize;              //4 bytes:File size in bytes
    uint32_t reserved;         //4 bytes: unused = 0
    uint32_t dataOffset;        //4 bytes: File offset to Raster Data
    uint32_t size;                  //4 bytes: size of infoHeader = 40
    int32_t widthPX;             //4 bytes: Bitmap Width
    int32_t heigthPX;           //4 bytes:Bitmap Heigth
    uint16_t num_planes;    //2 bytes: Number of planes
    uint16_t bitCount;          /*2 bytes: Bits per Pixel1 = monochrome palette. NumColors = 1  
                                            4 = 4bit palletized. NumColors = 16  
                                            8 = 8bit palletized. NumColors = 256 
                                            16 = 16bit RGB. NumColors = 65536 (?) 
                                            24 = 24bit RGB. NumColors = 16M*/
    uint32_t compression;  //4 bytes: Type of Compression:
                                            /*0 = BI_RGB   no compression  
                                            1 = BI_RLE8 8bit RLE encoding  
                                            2 = BI_RLE4 4bit RLE encoding*/
    uint32_t ImageSize;     //4 bytes:(compressed) Size of Image 
                                           //It is valid to set this =0 if Compression = 0
    int32_t XpixlesPerM;    //4 bytes: Horizontal resulotion: Pixels/meter
    int32_t YpixlesPerM;    //4 bytes: Vertical resolution: Pixels/meter
    uint32_t colorUsed;     //4 bytes:Number of actually used colors
    uint32_t colorsImport; //4 bytes: Number of Important colors 0 = all
    
}BMP_Header;

#pragma pack(pop)

void LooKujund()
{
    
}
void JoonistaMaluPilt(uint8_t ** image, int width, int height, int boardSize,int kuju)
{
    int pixleOffset = 0;
    int kujuX;
    int kujuY;
    int dx;
    int dy;
    int radius;
    *image = (uint8_t*)malloc(3*width*height);

    if(*image == NULL)
    {
        printf("Ei saanud eraldada malu\n");
        exit(0);
    }
    switch(kuju)
    {
        //kolmnurk
        case 1:
            kujuX = width /2;
            kujuY = width/2;
            break;
        //nelinurk
        case 2:
            kujuX = width/2;
            kujuY = width/2;
            radius =  (width < height ? width : height) /2;
            break;
        //viisnurk
        case 3:
            kujuX = width/2;
            kujuY = width/2;
            radius = (width < height ? width : height) /2;
            break;
        //ring
        case 4:
            kujuX = width /2;
            kujuY = width/2;
            radius = (width < height ? width : height) /2;
            break;
    
    }
    for(int y = boardSize; y < height - boardSize;y++)
    {
      for(int x = boardSize; x < width - boardSize;x++)
      {
          uint8_t r, g ,b; //red,blue,green(RGB)
          //pixleOffset = (y * width + x ) * 3;
          switch(kuju)
          {
              //kolmnurk
              case 1:
               
                pixleOffset = 3* (x * width +y );
                if(y < x)
                {
                    r = x * 255 / height;
                    g = 255 - (x * 255 / height);
                    b = 0;
                    (*image)[pixleOffset] = b;
                    (*image)[pixleOffset + 1] = g;
                    (*image)[pixleOffset + 2] = r;
                }
                break;
            //nelinurk
            case 2:
                dx = abs(y - kujuY);
                dy = abs(x - kujuX);
                if(dx <= radius && dy <=radius)
                {
                    pixleOffset = 3 *( y *width +x);
                    r = 255 - (x * 255 / height);
                    g = 0;
                    b = x * 255 / height;
                    (*image)[pixleOffset] = b;
                    (*image)[pixleOffset + 1] = g;
                    (*image)[pixleOffset + 2] = r;
                }
                break;
            //viisnurk
            case 3:
            dx = y - kujuY;
            dy = x - kujuX;
            double angle = atan2(dy,dx);
            double distance1 = sqrt(dx*dx+dy*dy);
                if (distance1 <= radius && (angle >= -M_PI / 5 && angle <= 3 * M_PI / 5)) 
                {
                     pixleOffset = 3 * (y * width + x);
                    r = x * 255 / height;
                    g = 0;
                    b = 255 - (x * 255 / height);
                    (*image)[pixleOffset] = b;
                    (*image)[pixleOffset + 1] = g;
                    (*image)[pixleOffset + 2] = r;
                }
                break;
            //raadius
            case 4:
                dx = y - kujuY;
                dy = x - kujuX;
                double distance = sqrt(dx * dx + dy * dy);
                if(distance <= radius)
                {
                    r = 0;
                    g = x * 255 / height;
                    b = 255 - (x * 255 / height);
                    pixleOffset = 3*( x * width + y);
                    (*image)[pixleOffset] = b;
                    (*image)[pixleOffset + 1] = g;
                    (*image)[pixleOffset + 2] = r;
                }
               
                break;
          }
      
      }  
    }
    
}

void SalvestaBMP(FILE* f, uint8_t *image,int height,int width)
{
    BMP_Header andmed;
    memset(&andmed,0,sizeof(andmed));
    andmed.signature = 0x4D42;  // 'BM'
    andmed.fileSize = sizeof(BMP_Header) + width * height * 3;
    andmed.reserved = 0;
    andmed.dataOffset = sizeof(BMP_Header);
    andmed.size = 40;
    andmed.widthPX = width;
    andmed.heigthPX = height;
    andmed.num_planes = 1;
    andmed.bitCount = 24; // 24 bit (RGB)red,green,blue
    andmed.compression = 0; //BI_RGB = no 
    andmed.ImageSize = 0; //set to 0 when compression is set to 0
    andmed.XpixlesPerM = 2835; //Pixles per meter
    andmed.YpixlesPerM = 2835;
    andmed.colorUsed = 0;
    andmed.colorsImport = 0; //0 = all
    
    fwrite(&andmed, sizeof(BMP_Header), 1, f);
    uint32_t padding = (4 - (width * 3) %4)%4;//calculate the process of adding one
                                                                            //or more empty bytes between the different data types
                                                                            //to align data in memory
    for(int i = 0; i <  height;i++)
    {
        fwrite(&image[i * width * 3],3,width,f);
        if(padding > 0)
        {
            uint8_t padBytes = 0;
            fwrite(&padBytes,1,padding,f);
        }
    }

}
int main(void)
{
    //printf("%ld\n",sizeof(BMP_Header));//Kontrolliks BMP suurust, 54 byte
    char dstFile[BUFFLEN];
    int kuju;
    int height;
    int width;
    int boardSize = 10; //miinimum 10 pixlit servast
    printf("Sisesta faili nimi: ");
    fgets(dstFile,BUFFLEN,stdin);
    while(dstFile[strlen(dstFile)-1] == '\n' || dstFile[strlen(dstFile)-1] == '\r')
    {
        dstFile[strlen(dstFile)-1] = '\0'; // lõpust reavahetus ära
    }
    if(strlen(dstFile)>MAX_USER_INP)
    {
        printf("Sisestus fail liiga suur\n");
        exit(0);
    }
    strcat(dstFile,ENDING);
    FILE *f = fopen(dstFile, "wb");
    if(f == NULL)
    {
        printf("Ei saanud avada faili '%s'\n",dstFile);
        return 1;
    }
    printf("Sisesta kõrgus 30 - 700: ");
    if(scanf("%d",&height) < 1)
    {
        printf("viga kõrgus\n");
        return 1;
    }
    if(height < 30 || height > 700)
    {
        printf("Kõrgus on parameetritest väljas\n");
        return 1;
    }
    while (getchar() != '\n');
    printf("Sisesta pikkus 30 - 700: ");
    if(scanf("%d",&width) < 1)
    {
        printf("Viga pikkuses\n");
        return 1;
    }
    if(width < 30 || width > 700)
    {
        printf("Pikkus on parameetritest väljas\n");
        return 1;
    }
    while (getchar() != '\n');
    printf("vali kujund: 1 = kolmnurk, 2 = nelinurk, 3 = poolring, 4 = ring, \n");
    if(scanf("%d",&kuju) < 1)
    {
        printf("ei saanud lugeda\n");
    }
    while(getchar() != '\n');
    if(kuju < 1 && kuju > 4)
    {
        printf("Kujund on vale\nDefault kujund - 1=kolmnurk\n");
        kuju = 1;
    }
 
    //Loo vastava suurusega ja kujundiga pildi
    uint8_t *image = NULL;
    JoonistaMaluPilt(&image,width,height,boardSize,kuju);
    //salvesta pilt
    SalvestaBMP(f,image,width,height);
    printf("Pilt on savlestatud nimega '%s'\n",dstFile);
    
    fclose(f);
    free(image);
    return 0;
}
